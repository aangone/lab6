/*
  Anthony Angone
  aangone
  Lab5
  Lab Section 2
  TA: Hanjie Liu
*/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"

using namespace std;
//enumerated values for the suits
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

//struct to contain the card
typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
 /*This is to seed the random generator */
  srand(unsigned (time(0)));

  //create an array of 52 card structs
  Card deck[52];
  int val = 2, suit = 0;
  //loop through creating 52 cards
  for(int i = 0; i<53; i++){
    //set the value of the card to the next value
    deck[i].value = val;
    //decide which suit the card should have
    switch(suit){
    case 0:
      deck[i].suit = SPADES;
      break;
    case 1:
      deck[i].suit = HEARTS;
      break;
    case 2:
      deck[i].suit = DIAMONDS;
      break;
    case 3:
      deck[i].suit = CLUBS;
      break;
    }
    val++;
    //if all values of one suit have been reached
    //reset the values to 2 and go to next suit
    if(val >= 15){
      val = 2;
      suit++;
    }
    
  }

  //randomize the order of the deck array
  random_shuffle(deck, (deck+53), myrandom);

  //initialize card array of 5 for the hand
  Card hand[5];
  //loop through and make the hand be equal to the first 5 in deck
  for(int i = 0; i < 5; i++){
    hand[i].value = deck[i].value;
    hand[i].suit = deck[i].suit;
  }

  //sort the hand based on suit_order
  sort(hand, (hand+5), suit_order);
    
  //iterate through hand and print each card
  for(auto x : hand){
    cout << setw(10) << get_card_name(x);
    cout << " of " + get_suit_code(x) << endl;
  }
  return 0;
}


/*Parameter: two references to a card struct
  Returns: Bool on if the first card is ordered before the second card
  Function: provides a way to order the card structs
 */
bool suit_order(const Card& lhs, const Card& rhs) {
  if(lhs.suit < rhs.suit) return true;
  if(lhs.suit == rhs.suit){
    if(lhs.value < rhs.value) return true;
  }
  return false;
}
/*Parameters: reference to a card struct
  Returns: a string of the unicode symbol for the suit
  Function: reads the suit and gets the appropriate symbol
 */
string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

/*Parameters: reference to a card struct
  Returns: string of the value of the card
  Function: takes a card and returns the value of the card
 */
string get_card_name(Card& c) {
  if(c.value <= 10){
    return to_string(c.value);
  }
  if(c.value == 11){
    return "Jack";
  }
  if(c.value == 12){
    return "Queen";
  }
  if(c.value == 13){
    return "King";
  }
  if(c.value == 14){
    return "Ace";
  }
  return "";
}
